Research project

# 'Modern state-owned firms' performance: An empirical analysis of productivity, market power, and innovation'
financed by the German Science Foundation (DFG) | 2019-2022

Author of the following codes: Julia Rechlitz (@jrechlitz)

All codes here provided are open source. All codes are licenced under the [MIT License](https://spdx.org/licenses/MIT.html) expanded by the [Commons Clause](https://commonsclause.com/). All data sets are licenced under the [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## Data preparation: Data on the German distribution system operators

The following r scripts construct a panel data set on Gernam electricity distribution system operators (DSOs). 
The final data set is based on the data base _(Netznutzung Strom)_ provided from the ene't company. 

[Click here]( https://download.enet.eu/download/ausliefern/quelle/198/nns-access-2021-07-01.pdf) for the data documentation of the complete data base _(Netznutzung Strom)_ 

 
**First step: Create the panel data set**

------------------------------------------------
The data on the German distribution system operators is obtained by the following data sets from 
the data base _(Netznutzungstrom)_: 

- Data set Netzbetreiber
- Data set Netze
- Data set Netzdaten

<details><summary>Click here to see a full list of the variables that we use.</summary>

| Variable | Description | Unit | Available years | Source |
| --------- | ---------- | -------- | -------- |  -------- |
|Einwohnerzahl|	Total number of inhabitants of the area served| | 2005-2016 |Data base Netznutzung Strom |	
|versFlaeche_NS| Supplied area of the low voltage network|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|versFlaeche_MS|	Supplied area of the medium voltage network|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|versFlaeche_HS|	Supplied area of the high voltage network|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_NS|	Geographical area of the low-voltage network|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_MS|	Geographical area of the medium voltage network|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_HS|	Geographical area of the high voltage network| 	km^2 | 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_NS|	Number of connection points in the low-voltage network|	| 2005-2016 |Data base Netznutzung Strom | 
|Entnahmestellen_MS_NS|	Number of connection points at the medium-voltage to low-voltage transformer level|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_MS|	Number of connection points in the medium-voltage grid|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HS_MS|	Number of connection points of the high voltage to medium voltage transformer level|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HS|	Number of connection points in the high-voltage grid|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HoeS_HS|	Number of connection points of the extra-high voltage to high voltage transformer level|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HoeS|	Number of connection points in the extra-high voltage grid|	| 2005-2016 |Data base Netznutzung Strom |
|Arbeit_NS|	Annual work withdrawn from the low-voltage level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_MS_NS|	Annual energy drawn from the medium-voltage to low-voltage transformer level| MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_MS|	Annual work drawn from the medium-voltage level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HS_MS|	Annual energy drawn from the high voltage to medium voltage transformer level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HS|	Annual work withdrawn from the high-voltage level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HoeS_HS|	Annual work withdrawn from the extra-high voltage to high voltage transformer level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HoeS|	Annual work taken from the peak voltage level|	MWh | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_MS_NS|	Installed power of transformers from medium voltage to low voltage|	MVA | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_HS_MS|	Installed power of transformers from high voltage to medium voltage|	MVA | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_HoeS_HS| Installed power of transformers from extra high voltage to high voltage to low voltage|	MVA | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_NS_Ka|	Circuit length of the cables in the low-voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_MS_Ka|	Circuit length of the cables in the medium voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HS_Ka|	Circuit length of cables in the high-voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HoeS_Ka|	Circuit length of cables in the extra-high voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_NS_Fr|	Circuit length of overhead lines in the low-voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_MS_Fr|	Circuit length of overhead lines in the medium-voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HS_Fr|	Circuit length of overhead lines in the high-voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HoeS_Fr|	Circuit length of overhead lines in the extra-high voltage level (incl. house connections)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_Ha_Fr|	Circuit length of overhead lines for house connections|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_Ha_Ka|	Circuit length of cables for house connections|	km| 2005-2016 |Data base Netznutzung Strom |
|Verluste_NS|	Average losses of the low-voltage level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_MS_NS|	Average losses of the medium-voltage to low-voltage transformer level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_MS|	Average losses of the medium voltage level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_HS_MS|	Average losses of the high-voltage to medium-voltage transformer level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_HS|	Average losses of the high-voltage level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_HoeS_HS|	Average losses of the extra-high voltage to high voltage transformation level|	% | 2005-2016 |Data base Netznutzung Strom |
|Verluste_HoeS|	Average losses of the extra-high voltage level|	% | 2005-2016 |Data base Netznutzung Strom |
|Beschaffungskost|	Procurement costs of network losses|	ct/kWh | 2005-2016 |Data base Netznutzung Strom |
|d_bld1	|Network in Schleswig-Holstein	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld2|	Network in Hamburg	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld3|	Network in Niedersachsen	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld4|	Network in Bremen	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld5|	Network in Nordrhein-Westfalen	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld6| Network in Hessen |	binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld7|	Network in Rheinland-Pfalz |	binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld8|	Network in Baden-Württemberg|	binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld9|	Network in Bayern	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld10|	Network in Saarland|	binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld11|	Network in Berlin	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld12|	Network in Brandenburg	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld13|	Network in Mecklenburg-Vorpommern	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld14|	Network in Sachsen|	binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld15|	Network in Sachsen-Anhalt	|binary | 2005-2016 |Data base Netznutzung Strom |
|d_bld16|	Network in Thüringen|	binary | 2005-2016 |Data base Netznutzung Strom |
|Recherche|	Indicator whether the register number has been added |	binary | 2005-2016 |Data base Netznutzung Strom |
|year	|Year	| | 2005-2016 |Data base Netznutzung Strom |
|Register_ort|	Place of registration in the commercial or cooperative register coded as city name| | 2005-2016 |Data base Netznutzung Strom | 
|Einwohnerzahl_im|	Total number of inhabitants of the area served (missings imputiert)|	 | 2005-2016 |Data base Netznutzung Strom | 
|versFlaeche_NS_im|	Supplied area of the low voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|versFlaeche_MS_im|	Supplied area of the medium voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|versFlaeche_HS_im|	Supplied area of the high voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_NS_im| Geographical area of the low-voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_MS_im|	Geographical area of the medium voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|geographFlaeche_HS_im|	Geographical area of the high voltage network (missings imputiert)|	km^2 | 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_NS_im|	Number of connection points in the low-voltage gridz (missings imputiert)|	| 2005-2016 |Data base Netznutzung Strom | 
|Entnahmestellen_MS_NS_im|	Number of connection points at the medium-voltage to low-voltage transformer level (missings imputiert)|	| 2005-2016 |Data base Netznutzung Strom | 
|Entnahmestellen_MS_im|	Number of connection points in the medium-voltage grid (missings imputiert)| | 2005-2016 |Data base Netznutzung Strom |	
|Entnahmestellen_HS_MS_im| Number of connection points of the high voltage to medium voltage transformer level (missings imputiert)	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HS_im|	Number of connection points in the high-voltage grid (missings imputiert)| | 2005-2016 |Data base Netznutzung Strom |	
|Entnahmestellen_HoeS_HS_im|	Number of connection points of the extra-high voltage to high voltage transformer level (missings imputiert)|	| 2005-2016 |Data base Netznutzung Strom |
|Entnahmestellen_HoeS_im|	Number of connection points in the extra-high voltage grid (missings imputiert)| | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_NS_im|	Annual work withdrawn from the low-voltage level (missings imputiert)|	MWh| 2005-2016 |Data base Netznutzung Strom |
|Arbeit_MS_NS_im|	Annual work withdrawn from the medium-voltage to low-voltage transformer level (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_MS_im|	Annual work drawn from the medium-voltage level (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HS_MS_im|	Annual energy drawn from the high voltage to medium voltage transformer leve (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HS_im|	Annual work withdrawn from the high-voltage level (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HoeS_HS_im|	Annual work withdrawn from the extra-high voltage to high voltage transformer level (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|Arbeit_HoeS_im|	Annual work taken from the peak voltage level (missings imputiert)|	MWh | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_MS_NS_im|	Installed power of transformers from medium voltage to low voltage (missings imputiert)|	MVA | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_HS_MS_im|	Installed power of transformers from high voltage to medium voltage (missings imputiert)|	MVA | 2005-2016 |Data base Netznutzung Strom |
|instLeistTrafo_HoeS_HS_im|	Installed power of transformers from extra high voltage to high voltage to low voltage|	MVA | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_NS_Ka_im|	Circuit length of the cables in the low-voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_MS_Ka_im|	Circuit length of the cables in the medium voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HS_Ka_im|	Circuit length of cables in the high-voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HoeS_Ka_im|	Circuit length of cables in the extra-high voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_NS_Fr_im|	Circuit length of overhead lines in the low-voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_MS_Fr_im|	Circuit length of overhead lines in the medium-voltage level (incl. house connections) (missings imputiert)|	km| 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HS_Fr_im|	Circuit length of overhead lines in the high-voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_HoeS_Fr_im|	Circuit length of overhead lines in the extra-high voltage level (incl. house connections) (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_Ha_Fr_im|	Circuit length of overhead lines for house connections (missings imputiert)|	km | 2005-2016 |Data base Netznutzung Strom |
|StromKreisLaenge_Ha_Ka_im|	Circuit length of cables for house connections (missings imputiert)|	km| 2005-2016 |Data base Netznutzung Strom |
|Verluste_NS_im|	Average losses of the low-voltage level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_MS_NS_im|	Average losses of the medium-voltage to low-voltage transformer level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_MS_im|	Average losses of the medium voltage level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_HS_MS_im|	Average losses of the high-voltage to medium-voltage transformer level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_HS_im|	Average losses of the high-voltage level (missings imputiert)	|%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_HoeS_HS_im|	Average losses of the extra-high voltage to high voltage transformation level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Verluste_HoeS_im|	Average losses of the extra-high voltage level (missings imputiert)|	%| 2005-2016 |Data base Netznutzung Strom |
|Beschaffungskost_im|	Procurement costs of network losses (missings imputiert)|	ct/kWh| 2005-2016 |Data base Netznutzung Strom |

</details>

 
**Second step: Re-code the information on the registry court**

--------------------------------------------------------------
The script prepares the _(ene't panel)_ for the link with the _(AFiD data)_. The link
happens via the commercial or cooperative register number, the type of 
register and the seat of the register court.


