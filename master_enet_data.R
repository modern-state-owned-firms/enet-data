#==============================================================================
# Master file
#
# This code uses the data sets X and Y and create a panel data set. In a second 
# step it adapts the firm specific information out of the Handelsregister to be 
# able to merge to merge the enet data with the AFiD panel    
#
# Authors: Julia Rechlitz                                                                                    
#                                                                                    
# Calls the following R code:
# "01_enet_panel_construction.R"
# "02_enet_adaption_handelsregister_information.R"
#==============================================================================

#==============================================================================
# 1. Set up R
#==============================================================================

# clean memory 
#--------------
rm(list = ls())

# Tell R to use fixed notation instead of exponential notation
#-------------------------------------------------------------
options(scipen = 999)


#==============================================================================
# 2. Define working environment
#==============================================================================

  datenpfad         <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/afid-data/enet-data/data"
  syntaxpfad        <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/afid-data/enet-data" 
  outputpfad        <- "C:/Users/julia/Documents/Promotion/Projekte/gitLab/afid-data/enet-data/output"
  
  dateiname_enet1   <- "nns_csv_windows_1252_Netzbetreiber.csv"
  dateiname_enet2   <- "nns_csv_windows_1252_Netze.csv"
  dateiname_enet3   <- "nns_csv_windows_1252_Netzdaten.csv"
  dateiname_enet4   <- "firms_wo_registerinfos_v05_14012021.csv"
  outputname        <- "log_create_enet_panel" 
  syntaxname1       <- "01_enet_panel_construction"
  syntaxname2       <- "02_enet_adaption_handelsregister_information"
  

#==============================================================================
# 3. Call different R codes
#==============================================================================

# start the log file
#---------------------
sink(paste(outputpfad, "/", outputname, ".log", sep = ""), append = FALSE, type = c("output", "message"), split = TRUE)


# execute R codes
#-----------------
source(paste(syntaxpfad, "/", syntaxname1, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)
source(paste(syntaxpfad, "/", syntaxname2, ".R", sep = ""), echo = TRUE, max.deparse.length = 99999)


# close the log file
#-------------------
sink()

